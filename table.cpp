#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;



int main()
{
	const int YEAR_MIN = 5;
	const int YEAR_MAX = 30;
	const int YEAR_INCR = 5;

	const double RATE_MIN = 5;
	const double RATE_MAX = 10;
	const double RATE_INCR = 0.5;

	cout << "Rate ";
	int year;

	for (year = YEAR_MIN; year <= YEAR_MAX; year = year + YEAR_INCR)

		cout << setw(2) << year << " years ";

		cout << "\n";
		cout << fixed << setprecision(2);

		double rate;
		double initial_balance = 1000;

		for (rate = RATE_MIN; rate <= RATE_MAX; rate = rate + RATE_INCR)
		{
			int year;
			cout << setw(5) << rate;

			for (year = YEAR_MIN; year <= YEAR_MAX; year = year + YEAR_INCR)
			{
				double balance = initial_balance * pow(1 + rate / 100, year);
				cout << setw(10) << balance;
			}
			cout << "\n";
		}
		cin.get();
		return 0;
}